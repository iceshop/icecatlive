<?php

namespace ICEShop\Icecatlive\Block\Adminhtml\System\Config\Form;

use Magento\Framework\Config\ConfigOptionsListConstants;

/**
 * Class Statistics
 * @package ICEShop\Icecatlive\Block\Adminhtml\System\Config\Form
 */
class Statistics extends \Magento\Config\Block\System\Config\Form\Field
{

    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('iceshop/icecatlive/statistics.phtml');
    }

    /**
     * Return element html
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return $import_info array for table of statistics
     *
     * @return string
     */

    public function collectData()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $deployConfig = $objectManager->get('Magento\Framework\App\DeploymentConfig');
        $tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);
        
        $DB_loger = $objectManager->get('\ICEShop\Icecatlive\Helper\Db');
        $import_info['startdate_imported_product'] = $DB_loger->getLogValue('icecatlive_startdate_imported_product');
        $import_info['startdate_update_product'] = $DB_loger->getLogValue('icecatlive_startdate_update_product');

        $import_info['endtdate_imported_product'] = $DB_loger->getLogValue('icecatlive_enddate_imported_product');
        $import_info['enddate_update_product'] = $DB_loger->getLogValue('icecatlive_enddate_update_product');

        $import_info['success_imported_product'] = $DB_loger->getLogValue('icecatlive_success_imported_product');
        $import_info['error_imported_product'] = $DB_loger->getLogValue('icecatlive_error_imported_product');
        $import_info['full_icecat_product'] = $DB_loger->getLogValue('icecatlive_full_icecat_product');
        $count_products = $DB_loger->readQuery("SELECT COUNT(*) FROM `" . $tablePrefix . "catalog_product_entity` LEFT JOIN `" . $tablePrefix . "iceshop_icecatlive_products_titles` ON entity_id = prod_id WHERE prod_id IS NULL");
        $import_info['count_not_updated_products'] = $count_products[0]['COUNT(*)'];
        if (file_exists(BP . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'iceshop/icecatlive/cache/') && is_readable((BP . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'iceshop/icecatlive/cache/'))) {
            $cache_files = scandir(BP . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'iceshop/icecatlive/cache/');
            $count_cache_files = count($cache_files) - 2;
        } else {
            $count_cache_files = 0;
        }
        $import_info['count_cache_files'] = $count_cache_files;
        return $import_info;
    }
}