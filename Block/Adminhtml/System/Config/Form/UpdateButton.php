<?php

namespace ICEShop\Icecatlive\Block\Adminhtml\System\Config\Form;

/**
 * Class UpdateButton
 * @package ICEShop\Icecatlive\Block\Adminhtml\System\Config\Form
 */
class UpdateButton extends \Magento\Config\Block\System\Config\Form\Field
{

    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('iceshop/icecatlive/ajaxstatusimport.phtml');
    }

    /**
     * Return element html
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

        $userLogin = $scopeConfig->getValue('icecat_root/icecat/login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $userPass = $scopeConfig->getValue('icecat_root/icecat/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (!empty($userLogin) && !empty($userPass)) {
            $prod_button = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
                ->setData(array(
                    'id' => 'icecatlive_button',
                    'label' => 'Update new products',
                    'onclick' => 'javascript:import_prod_info(1); return false;'
                ));
            $buttons = $prod_button->toHtml();
            return $buttons;
        } else {
            return 'Please type your login and pass from IceCat and press Save Config button. After that you can see Import button, for start import data from IceCat to you DB.';
        }
    }
}