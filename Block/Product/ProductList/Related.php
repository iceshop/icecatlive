<?php

namespace ICEShop\Icecatlive\Block\Product\ProductList;

/**
 * Class Related
 * @package ICEShop\Icecatlive\Block\Product\ProductList
 */

class Related extends \Magento\Catalog\Block\Product\ProductList\Related {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \ICEShop\Icecatlive\Helper\Getdata
     */
    protected $icecatliveGetdataHelper;

    /**
     * @var \ICEShop\Icecatlive\Model\Relatedcollection
     */
    protected $icecatliveRelatedcollection;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\ResourceModel\Cart $checkoutCart,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \ICEShop\Icecatlive\Helper\Getdata $icecatliveGetdataHelper,
        \ICEShop\Icecatlive\Model\Relatedcollection $icecatliveRelatedcollection,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        array $data = []
    ) {
        $this->registry = $context->getRegistry();
        $this->icecatliveGetdataHelper = $icecatliveGetdataHelper;
        $this->icecatliveRelatedcollection = $icecatliveRelatedcollection;
        $this->catalogProductVisibility = $catalogProductVisibility;

        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        parent::__construct(
            $context,
            $checkoutCart,
            $catalogProductVisibility,
            $checkoutSession,
            $moduleManager,
            $data
        );
    }

    protected function _prepareData()
    {
        $helper = $this->icecatliveGetdataHelper;
        $relatedProducts = $helper->getRelatedProducts();
        if (!$relatedProducts) {
            return parent::_prepareData();
        }

        $tmp = new \ICEShop\Icecatlive\Model\Relatedcollection($relatedProducts);

        $tmp = $tmp->getCollection();

        $this->_itemCollection = $tmp;

        $this->_addProductAttributesAndPrices($this->_itemCollection);
        $this->_itemCollection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        $this->_itemCollection->load();
        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

}
