<?php

namespace ICEShop\Icecatlive\Block\Product\ProductList;

use Magento\Framework\Config\ConfigOptionsListConstants;

/**
 * Class Toolbar
 * @package ICEShop\Icecatlive\Block\Product\ProductList
 */

class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{
    public static $_productCollection = null;
    public static $_totalRecords = null;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    public $tablePrefix;

    public $objectManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Catalog\Model\Product\ProductList\Toolbar $toolbarModel,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Catalog\Helper\Product\ProductList $productListHelper,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        $data,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->scopeConfig = $this->objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');

        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $deployConfig = $this->objectManager->get('Magento\Framework\App\DeploymentConfig');
        $this->tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);

        if(is_null($data)) {
            $data = [];
        } elseif (!is_array($data)) {
            $data = [$data];
        }

        parent::__construct(
            $context,
            $catalogSession,
            $catalogConfig,
            $toolbarModel,
            $urlEncoder,
            $productListHelper,
            $postDataHelper,
            $data
        );
    }
    public function getCollection()
    {

        $productPriority = $this->scopeConfig->getValue('icecat_root/icecat/product_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_productCollection = parent::getCollection();

        if (!$_productCollection->count() || $productPriority == 'Show' || $productPriority == '') {
            return $_productCollection;
        } else {
            foreach ($_productCollection as $_product) {
                $icecat_prod = $this->checkIcecatData($_product);
                if ($icecat_prod === false) {
                    $_productCollection->removeItemByKey($_product->getId());
                }
            }

            self::$_productCollection = $_productCollection;

            return $_productCollection;
        }
    }

    public function getTotalNum()
    {
        if (self::$_productCollection === null) {
            return parent::getTotalNum();
        }
        self::$_totalRecords = count(self::$_productCollection->getItems());
        return intval(self::$_totalRecords);
    }

    public function checkIcecatData($_product)
    {
        $tablePrefix = $this->tablePrefix;

        $db_res = $this->resourceConnection->getConnection('core_write');
        $query = "SELECT `entity_id` FROM `" . $tablePrefix . "catalog_product_entity` LEFT JOIN `"
                . $tablePrefix . "iceshop_icecatlive_products_titles` ON entity_id = prod_id WHERE prod_id IS NOT NULL";
        $entity_id = $db_res->fetchAll($query);
        return in_array(array('entity_id' => $_product->getId()), $entity_id);

    }
}
