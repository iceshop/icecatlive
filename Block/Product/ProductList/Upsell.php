<?php

namespace ICEShop\Icecatlive\Block\Product\ProductList;

/**
 * Class Upsell
 * @package ICEShop\Icecatlive\Block\Product\ProductList
 */

class Upsell extends \Magento\Catalog\Block\Product\ProductList\Upsell
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Checkout\Model\ResourceModel\Cart $checkoutCart,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        array $data = []
    ) {
        $this->registry = $context->getRegistry();
        $this->scopeConfig = $context->getScopeConfig();
        $this->checkoutSession = $checkoutSession;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->eventManager = $context->getEventManager();

        parent::__construct(
            $context,
            $checkoutCart,
            $catalogProductVisibility,
            $checkoutSession,
            $moduleManager,
            $data
        );
    }
    protected function _prepareData()
    {
        $product = $this->registry->registry('product');

        $this->_itemCollection = $product->getUpSellProductCollection()
            ->addAttributeToSort('position', 'asc')
            ->addStoreFilter();
        $skuField = $this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->_itemCollection->addAttributeToSelect($skuField);

        $manufacturerId = $this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->_itemCollection->addAttributeToSelect($manufacturerId);

       /* Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
            $this->checkoutSession->getQuoteId()
        );*/

        $this->_addProductAttributesAndPrices($this->_itemCollection);

        $this->_itemCollection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        if ($this->getItemLimit('upsell') > 0) {
            $this->_itemCollection->setPageSize($this->getItemLimit('upsell'));
        }

        $this->_itemCollection->load();

        $this->eventManager->dispatch('catalog_product_upsell', array(
            'product' => $product,
            'collection' => $this->_itemCollection,
            'limit' => $this->getItemLimit()
        ));

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }
}
