<?php

namespace ICEShop\Icecatlive\Block\Product\View;

/**
 * Class Extra
 * @package ICEShop\Icecatlive\Block\Product\View
 */

class Extra extends \Magento\Catalog\Block\Product\View\Attributes

    {
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    public $registry;

    /**
     * @var \ICEShop\Icecatlive\Model\Import
     */
    public $icecatliveImport;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    public $catalogCategoryFactory;

    /**
     * @var \ICEShop\Icecatlive\Model\Observer
     */
    public $icecatliveObserverFactory;

    public $_product;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objectManager;

    /**
     * Extra constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \ICEShop\Icecatlive\Model\Import $icecatliveImport
     * @param \Magento\Catalog\Model\CategoryFactory $catalogCategoryFactory
     * @param \ICEShop\Icecatlive\Model\Observer $icecatliveObserverFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \ICEShop\Icecatlive\Model\Import $icecatliveImport,
        \Magento\Catalog\Model\CategoryFactory $catalogCategoryFactory,
        \ICEShop\Icecatlive\Model\Observer $icecatliveObserverFactory
    ) {
        $this->icecatliveObserverFactory = $icecatliveObserverFactory;
        $this->registry = $registry;
        $this->icecatliveImport = $icecatliveImport;
        $this->storeManager = $context->getStoreManager();
        $this->catalogCategoryFactory = $catalogCategoryFactory;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->scopeConfig = $context->getScopeConfig();

        $import = $this->icecatliveObserverFactory;
        $cache_file = $this->getCacheFile();

        if(!file_exists($cache_file) && $this->scopeConfig->getValue('icecat_root/icecat/product_loadingtype', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)){
            $this->_product = $import->loadProductInfoIntoCache((int)$this->registry->registry('current_product')->getId(),0,1);
        }

        $data = [];
        parent::__construct(
            $context,
            $registry,
            $priceCurrency,
            $data
        );
    }

    public function getCacheFile(){
        $import = $this->icecatliveObserverFactory;
        $locale = $this->scopeConfig->getValue('icecat_root/icecat/language', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($locale == '0') {
            $systemLocale = explode("_", $this->objectManager->get('\Magento\Framework\Locale\Resolver')->getLocale());
            $locale = $systemLocale[0];
        }
        return BP.DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . $import->_connectorCacheDir . 'iceshop_icecatlive_'. $this->registry->registry('current_product')->getId() .'_' . $locale;
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->registry->registry('product');
        }
        return $this->_product;
    }

    public function getAdditionalData(array $excludeAttr = [])
    {

        $data = $this->getAttributesArray($excludeAttr);

        $data2 = [];
        foreach ($data as $_data) {
            if ($_data['label'] != '' && $_data['value'] != '' && $_data['label'] != 'id') {
                $value = $_data['value'];
                $group = 0;
                if(!empty($_data["id"])){
                    $tmp = $_data["id"];
                } else {
                    $tmp = '';
                }
                if ($tmp) {
                    $group = $tmp;
                }

                $data2[$group]['items'][$_data['label']] = array(
                    'label' => $_data['label'],
                    'value' => $value,
                    'code' => $_data['label']
                );

                if(!empty($_data["id"])){
                    $data2[$group]['attrid'] = $_data["id"];
                } else {
                    $data2[$group]['attrid'] = '';
                }

            } else if (!empty($_data['code']) && $_data['code'] == 'header') {
                $data2[$_data['id']]["title"] = $_data['value'];
            }
        }

        return $data2;
    }

    public function formatValue($value)
    {
        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
            if ($value == "Y" || $value == "Yes" || $value == "YES") {
                return '<img border="0" alt="" src="https://prf.icecat.biz/imgs/yes.gif"/>';
            } else if ($value == "N" || $value == "NO" || $value == "No") {
                return '<img border="0" alt="" src="https://prf.icecat.biz/imgs/no.gif"/>';
            }
        }else{
            if ($value == "Y" || $value == "Yes" || $value == "YES") {
                return '<img border="0" alt="" src="http://prf.icecat.biz/imgs/yes.gif"/>';
            } else if ($value == "N" || $value == "NO" || $value == "No") {
                return '<img border="0" alt="" src="http://prf.icecat.biz/imgs/no.gif"/>';
            }
        }
        return str_replace("\\n", "<br>", htmlspecialchars($value));
    }

    public function getAttributesArray($excludeAttr = '')
    {
        $iceModel = $this->icecatliveImport;

        $descriptionsListArray = $iceModel->getProductDescriptionList();

        $id = '';
        $arr = [];
        foreach ($descriptionsListArray as $key => $ma) {
            $id = $key;
            foreach ($ma as $key => $value) {
                $arr[$key] = $value;
                $arr[$key]["id"] = $id;
            }
        }

        $data = [];
        $product = $this->getProduct();
        $attributes_general = $product->getAttributes();
        foreach ($attributes_general as $attribute) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);
                if (!$product->hasData($attribute->getAttributeCode())) {
                    $value = __('N/A');
                } elseif ((string)$value == '') {
                    $value = __('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = $this->storeManager->getStore()->convertPrice($value, true);
                }
                if (is_string($value) && strlen($value)) {
                    if (!$this->isAttributeView($attribute->getAttributeCode())) {
                        $data[$attribute->getAttributeCode()] = [
                            'label' => $attribute->getStoreLabel(),
                            'value' => $this->formatValue($value),
                            'code' => $attribute->getAttributeCode()
                        ];
                    }
                }
            }
        }

        foreach ($arr as $key => $value) {
            $data[] = array(
                'label' => '',
                'value' => $key,
                'code' => 'header',
                'id' => $value["id"]
            );
            $attributes = $value;
            foreach ($attributes as $attributeLabel => $attributeValue) {
                $data[] = array(
                    'label' => $attributeLabel,
                    'value' => $this->formatValue($attributeValue),
                    'code' => 'descript',
                    'id' => $value["id"]
                );
            }
        }

        return $data;
    }

    public function isAttributeView($attribute){
        if(file_exists($this->getCacheFile())){
            $view_product_attributes = explode(",",$this->scopeConfig->getValue('icecat_root/icecat/view_attributes', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            return in_array($attribute, $view_product_attributes);
        }
        return false;
    }
}


