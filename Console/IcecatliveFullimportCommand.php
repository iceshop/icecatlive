<?php

namespace ICEShop\Icecatlive\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ICEShop\ICEImport\Cron\Load;

class IcecatliveFullimportCommand extends Command
{
    private $state;

    /**
     * @var \ICEShop\Icecatlive\Model\Observer
     */
    protected $icecatliveObserverFactory;


    protected function configure()
    {
        $this->setName('icecatlive:fullimport')->setDescription('Runs the IceCatLive full import.');
    }

    /**
     * IcecatliveFullimportCommand constructor.
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(\Magento\Framework\App\State $state, \ICEShop\Icecatlive\Model\Observer $icecatliveObserverFactory) {
        $this->state = $state;
        $this->icecatliveObserverFactory = $icecatliveObserverFactory;

        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

        $lock_file_path = MAGENTO_BP . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'icecatlive_fullimport_job.lock';

        if (file_exists($lock_file_path)) {
            $last_start_time = date('Y-m-d H:i:s', file_get_contents($lock_file_path));
            throw new \Exception(__("Another IceCatLive full import process is running. Process was started at: " . $last_start_time));
        } else {
            file_put_contents($lock_file_path, time());
        }

        try{
            do {
                $result = $this->icecatliveObserverFactory->loadProductInfoIntoCache(0,1);
            } while (isset($result['done']) && $result['done'] === 0);

        } catch (\Exception $e) {
            if (file_exists($lock_file_path)) {
                unlink($lock_file_path);
            }
            throw new \Exception($e->getMessage());
        }

        if(isset($result['count_products'])) $output->writeln('Count Products:'.$result['count_products']);
        if(isset($result['success_imported_product'])) $output->writeln('Success Imported Product:'.$result['success_imported_product']);
        if(isset($result['error_imported_product'])) $output->writeln('Error Imported Product:'.$result['error_imported_product']);
        if(isset($result['full_icecat_product'])) $output->writeln('Full Icecat Products:'.$result['full_icecat_product']);

        if (file_exists($lock_file_path)) {
            unlink($lock_file_path);
        }
    }
}