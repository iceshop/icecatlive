<?php

namespace ICEShop\Icecatlive\Controller\Adminhtml\Data;

/**
 * Class Importproductinfo
 * @package ICEShop\Icecatlive\Controller\Adminhtml\Data
 */

class Importproductinfo extends \Magento\Backend\App\Action
{

    /**
     * @var \ICEShop\Icecatlive\Model\Observer
     */
    protected $icecatliveObserverFactory;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $backendAuthSession;

    public function execute()
    {
        $this->checkAction();
    }

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \ICEShop\Icecatlive\Model\Observer $icecatliveObserverFactory,
        \Magento\Backend\Model\Auth\Session $backendAuthSession
    ) {
        $this->icecatliveObserverFactory = $icecatliveObserverFactory;
        $this->backendAuthSession = $backendAuthSession;
        parent::__construct($context);
    }
    /**
     * Return some checking result
     *
     * @return void
     */
    public function checkAction()
    {

        $result = $this->icecatliveObserverFactory->loadProductInfoIntoCache();
        $this->getResponse()->setBody($result);
    }

    protected function _isAllowed()
    {
        return $this->backendAuthSession->isAllowed('system/config/icecat_root');
    }
}