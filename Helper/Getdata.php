<?php

namespace ICEShop\Icecatlive\Helper;

/**
 * Class Getdata
 * @package ICEShop\Icecatlive\Helper
 */

class Getdata extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var
     */
    private $iceCatModel;
    private $error;
    private $systemError;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \ICEShop\Icecatlive\Model\Import
     */
    protected $icecatliveImport;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objectManager;

    public $entityTypeId;

    /**
     * Getdata constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \ICEShop\Icecatlive\Model\Import $icecatliveImport
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \ICEShop\Icecatlive\Model\Import $icecatliveImport
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->resourceConnection = $resourceConnection;
        $this->scopeConfig = $context->getScopeConfig();
        $this->icecatliveImport = $icecatliveImport;
        $this->logger = $context->getLogger();
        parent::__construct(
            $context
        );
    }

    /**
     * Gets product Data and delegates it to Model
     * @param \Magento\Catalog\Model\Product $_product
     * @return \ICEShop\Icecatlive\Helper\Getdata
     */

    /**
     * @return string
     */
    public function getEntityTypeId()
    {
        $connection = $this->resourceConnection->getConnection('core_read');
        $query = $connection->select()
            ->from($this->resourceConnection
                ->getTableName('eav_entity_type'), 'entity_type_id')
            ->where('entity_type_code = ?', 'catalog_product')
            ->limit(1);
        $this->entityTypeId = $connection->fetchOne($query);
        return $this->entityTypeId;
    }

    /**
     * @param $_product
     * @return $this
     */
    public function getProductDescription($_product)
    {

        try {
            $entityId = $_product->getEntityId();
            $entity_type_id = $this->getEntityTypeId();
            $connection = $this->resourceConnection->getConnection('core_read');
            $mpn = $_product->getData($this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            $ean_code = $_product->getData($this->scopeConfig->getValue('icecat_root/icecat/ean_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            $manufacturerId = $_product->getData($this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            $attributeInfo = $this->objectManager->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection')
                ->setCodeFilter($this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
                ->setEntityTypeFilter($entity_type_id)
                ->getFirstItem();
            if ($attributeInfo->getData('backend_type') == 'int' ||
                ($attributeInfo->getData('frontend_input') == 'select' && $attributeInfo->getData('backend_type') == 'static')
            ) {
                $attribute = $attributeInfo->setEntity($_product->getResource());
                $manufacturer = $attribute->getSource()->getOptionText($manufacturerId);
            } else {
                $manufacturer = $manufacturerId;
            }

            $locale = $this->scopeConfig->getValue('icecat_root/icecat/language', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if ($locale == '0') {
                $systemLocale = explode("_", $this->objectManager->get('\Magento\Framework\Locale\Resolver')->getLocale());
                $locale = $systemLocale[0];
            }
            $userLogin = $this->scopeConfig->getValue('icecat_root/icecat/login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $userPass = $this->scopeConfig->getValue('icecat_root/icecat/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $this->iceCatModel = $this->icecatliveImport;

            if (!$this->iceCatModel->getProductDescription($mpn, $manufacturer, $locale, $userLogin, $userPass, $entityId, $ean_code)) {
                $this->error = $this->iceCatModel->getErrorMessage();
                $this->systemError = $this->iceCatModel->getSystemError();
                return $this;
            }
        } catch (\Exception $e) {
            $this->logger->debug('Icecat getProductDescription error' . $e);
        }
        return $this;
    }

    /**
     * returns true if error during data fetch occured else false
     */
    public function hasError()
    {
        if ($this->error || $this->systemError) {
            return true;
        }
        return false;
    }

    /**
     * return error message
     */
    public function getError()
    {
    }

    /**
     * return system error
     */
    public function hasSystemError()
    {
        if ($this->systemError) {
            return $this->systemError;
        }
        return false;
    }

    public function getProductDescriptionList()
    {
        return $this->iceCatModel->getProductDescriptionList();
    }

    public function getShortProductDescription()
    {
        return $this->iceCatModel->getShortProductDescription();
    }

    public function getLowPicUrl()
    {
        return $this->iceCatModel->getLowPicUrl();
    }

    public function getThumbPicture()
    {
        return $this->iceCatModel->getThumbPicture();
    }

    public function getGalleryPhotos()
    {
        return $this->iceCatModel->getGalleryPhotos();
    }

    public function getProductName()
    {
        return $this->iceCatModel->getProductName();
    }

    public function getVendor()
    {
        return $this->iceCatModel->getVendor();
    }

    public function getFullProductDescription()
    {
        return $this->iceCatModel->getFullProductDescription();
    }

    public function getMPN()
    {
        return $this->iceCatModel->getMPN();
    }

    public function getEAN()
    {
        return $this->iceCatModel->getEAN();
    }

    /**
     * @return array
     */
    public function getRelatedProducts()
    {
        if (!$this->iceCatModel) {
            $this->iceCatModel = $this->icecatliveImport;
        }
        $relatedProducts = $this->iceCatModel->getRelatedProducts();
        if (empty($relatedProducts)) {
            return [];
        }
        $sku = $this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $collection = $this->objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\Collection');
        $filterArray = [];
        foreach ($relatedProducts as $mpn => $valueArray) {
            array_push($filterArray, array('attribute' => $sku, 'eq' => $mpn));
        }
        $collection->addFieldToFilter($filterArray);
        $collection->addAttributeToSelect($sku);
        $collection->addAttributeToSelect('category_ids');
        $relatedProductsList = [];
        foreach ($collection as $product) {
            $categoryIds = $product->getCategoryIds();
            if (!empty($categoryIds)) {
                if (is_array($categoryIds)) {
                    $catogoriesArray = $categoryIds;
                }
                if (is_string($categoryIds)) {
                    $catogoriesArray = explode(",", $product->getCategoryIds());
                }
                foreach ($catogoriesArray as $categoryId) {
                    if (!array_key_exists($product->getData($sku), $relatedProducts)) {
                        continue;
                    }
                    $relatedProductInfo = $relatedProducts[$product->getData($sku)];
                    $relatedProductInfo['mpn'] = $product->getData($sku);
                    $relatedProductInfo['url'] = preg_replace('/\/\d+\/$/', "/" . $categoryId . "/", $product->getProductUrl());;
                    if (!array_key_exists($categoryId, $relatedProductsList)) {
                        $relatedProductsList[$categoryId] = [];
                    }
                    array_push($relatedProductsList[$categoryId], $relatedProductInfo);
                }
            } else {
                if (!array_key_exists($product->getData($sku), $relatedProducts)) {
                    continue;
                }
                $relatedProductInfo = $relatedProducts[$product->getData($sku)];
                $relatedProductInfo['mpn'] = $product->getData($sku);
                $relatedProductInfo['url'] = preg_replace('/category\/\d+\/$/', '', $product->getProductUrl());;
                if (!array_key_exists('a', $relatedProductsList)) {
                    $relatedProductsList['a'] = [];
                }
                array_push($relatedProductsList['a'], $relatedProductInfo);
            }
        }
        return $relatedProductsList;
    }
}