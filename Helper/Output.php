<?php

namespace ICEShop\Icecatlive\Helper;

/**
 * Class Output
 * @package ICEShop\Icecatlive\Helper
 */

class Output extends \Magento\Catalog\Helper\Output
{

    private $iceCatModel;

    private $error = false;

    private $isFirstTime = true;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \ICEShop\Icecatlive\Model\Import
     */
    protected $icecatliveImport;

    /**
     * @var \ICEShop\Icecatlive\Helper\Getdata
     */
    protected $icecatliveGetdataHelper;

    /**
     * @var \ICEShop\Icecatlive\Model\Catalog\Product
     */
    protected $icecatliveCatalogProductFactory;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objectManager;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    public $_request;

    /**
     * Output constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Catalog\Helper\Data $catalogData
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\Registry $registry
     * @param \ICEShop\Icecatlive\Model\Import $icecatliveImport
     * @param Getdata $icecatliveGetdataHelper
     * @param \ICEShop\Icecatlive\Model\Catalog\Product $icecatliveCatalogProductFactory
     * @param \Magento\Framework\App\Request\Http $request
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Helper\Data $catalogData,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Registry $registry,
        \ICEShop\Icecatlive\Model\Import $icecatliveImport,
        \ICEShop\Icecatlive\Helper\Getdata $icecatliveGetdataHelper,
        \ICEShop\Icecatlive\Model\Catalog\Product $icecatliveCatalogProductFactory,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->icecatliveCatalogProductFactory = $icecatliveCatalogProductFactory;
        $this->registry = $registry;
        $this->scopeConfig = $context->getScopeConfig();
        $this->icecatliveImport = $icecatliveImport;
        $this->icecatliveGetdataHelper = $icecatliveGetdataHelper;
        $this->_request = $request;
        parent::__construct($context, $eavConfig, $catalogData, $escaper);

    }

    /**
     * Prepare product attribute html output
     *
     * @param   \Magento\Catalog\Model\Product $product
     * @param   string $attributeHtml
     * @param   string $attributeName
     * @return  string
     */

    public function productAttribute($product, $attributeHtml, $attributeName)
    {

        $productId = $product->getId();

        if (!$this->registry->registry('product')) {
            $this->registry->register('product', $product);
            $this->registry->register('current_product', $product);
        }

        if ($attributeName == 'image') {
            return parent::productAttribute($product, $attributeHtml, $attributeName);
        }

        $productDescriptionPriority = $this->scopeConfig->getValue('icecat_root/icecat/descript_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $productShortDescrPriority = $this->scopeConfig->getValue('icecat_root/icecat/shortdescr_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $productNamePriority = $this->scopeConfig->getValue('icecat_root/icecat/name_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $dbDescriptionPriority = false;
        $dbShortDescriptionPriority = false;

        $current_page = $this->_request->getFullActionName();

        if ($productDescriptionPriority == 'Db' && $attributeName == 'description') {
            $dbDescriptionPriority = true;
        }
        if ($productShortDescrPriority == 'Db' && $attributeName == 'short_description') {
            $dbShortDescriptionPriority = true;
        }

        if ($current_page == 'catalog_product_view') {

            $bin_prod = $this->icecatliveCatalogProductFactory;
            if ($attributeName == 'description' || $attributeName == 'short_description') {
                $descr = $bin_prod->checkIcecatProdDescription($productId, $attributeName);
            }
       }
        $prod_source = \ICEShop\Icecatlive\Model\Catalog\Product::$_product_source;

        if ($prod_source == 'DB' && empty($descr)) {
            $dbDescriptionPriority = true;
            $dbShortDescriptionPriority = true;
        }

        if ($dbShortDescriptionPriority || ($current_page != 'catalog_product_view'
                && $prod_source != 'DB') && $attributeName != 'name'
        ) {
            if ($attributeName == 'short_description') {
                $attributeHtml = $product->getData('short_description');
            }

            if ($attributeName == 'description' && $attributeHtml == 1) {
                $attributeHtml = $product->getData('description');
            }
            return parent::productAttribute($product, $attributeHtml, $attributeName);
        }

        $this->iceCatModel = $this->icecatliveImport;

        if ($this->isFirstTime) {

            $helper = $this->icecatliveGetdataHelper;
            $helper->getProductDescription($product);

            if ($helper->hasError() && $attributeName != 'name') {
                $this->error = true;
            }
            $this->isFirstTime = false;
        }

        if ($this->error) {
            if ($attributeName != 'description' && $attributeName != 'short_description') {
                return parent::productAttribute($product, $attributeHtml, $attributeName);
            } else {
                return '';
            }

        }

        $id = $product->getData('entity_id');

        if ($attributeName == 'name' && $productNamePriority != 'Db') {
            if ($product->getId() == $this->iceCatModel->entityId && $name = $this->iceCatModel->getProductName()) {
                return $name;
            } else if (!empty($descr)) {
                return $descr;
            }
            $manufacturerId = $this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $mpn = $this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $ean = $this->scopeConfig->getValue('icecat_root/icecat/ean_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $collection = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
            $collection->addAttributeToSelect($manufacturerId)->addAttributeToSelect($mpn)->addAttributeToSelect($ean)
                ->addAttributeToSelect('name')
                ->addAttributeToFilter('entity_id', array('eq' => $id))->load();
            $product = $collection->getFirstItem();

            return $product->getName();
        }

        if ($attributeName == 'short_description' && !$dbShortDescriptionPriority) {

            $icecat_descr = $this->iceCatModel->getShortProductDescription();
            if (!empty($descr)) {
                return $descr;
            } else if (!empty($icecat_descr)) {
                return $icecat_descr;
            } else {
                $attributeHtml = $product->getData('short_description');
                return parent::productAttribute($product, $attributeHtml, $attributeName);
            }
        }

        if ($attributeName == 'description' && !$dbDescriptionPriority) {

            $icecat_full_descr = $this->iceCatModel->getFullProductDescription();

            if (!empty($icecat_full_descr)) {
                return str_replace("\\n", "<br>", $icecat_full_descr);
            } else {
                $attributeHtml = $product->getData('description');
            }
        }
        return parent::productAttribute($product, $attributeHtml, $attributeName);
    }

    public function getWarrantyInfo()
    {
        return $this->iceCatModel->getWarrantyInfo();
    }

    public function getShortSummaryDescription()
    {
        return $this->iceCatModel->getShortSummaryDescription();
    }

    public function getLongSummaryDescription()
    {
        return $this->iceCatModel->getLongSummaryDescription();
    }

    public function getManualPDF()
    {
        return $this->iceCatModel->getManualPDF();
    }

    public function getPDF()
    {
        return $this->iceCatModel->getPDF();
    }

    public function getIceCatMedia()
    {
        $media = (array)$this->iceCatModel->getIceCatMedia();
        return (array_key_exists('@attributes', $media)) ? $media['@attributes'] : [];
    }
}