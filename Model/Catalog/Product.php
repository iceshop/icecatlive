<?php

namespace ICEShop\Icecatlive\Model\Catalog;

/**
 * Class Product
 * @package ICEShop\Icecatlive\Model\Catalog
 */
class Product extends \Magento\Catalog\Model\Product
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objectManager;

    /**
     * Product constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataService
     * @param \Magento\Catalog\Model\Product\Url $url
     * @param \Magento\Catalog\Model\Product\Link $productLink
     * @param \Magento\Catalog\Model\Product\Configuration\Item\OptionFactory $itemOptionFactory
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory $stockItemFactory
     * @param \Magento\Catalog\Model\Product\OptionFactory $catalogProductOptionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $catalogProductStatus
     * @param \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig
     * @param \Magento\Catalog\Model\Product\Type $catalogProductType
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Catalog\Helper\Product $catalogProduct
     * @param \Magento\Catalog\Model\ResourceModel\Product $resource
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $resourceCollection
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry
     * @param \Magento\Catalog\Model\Indexer\Product\Flat\Processor $productFlatIndexerProcessor
     * @param \Magento\Catalog\Model\Indexer\Product\Price\Processor $productPriceIndexerProcessor
     * @param \Magento\Catalog\Model\Indexer\Product\Eav\Processor $productEavIndexerProcessor
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Catalog\Model\Product\Image\CacheFactory $imageCacheFactory
     * @param \Magento\Catalog\Model\ProductLink\CollectionProvider $entityCollectionProvider
     * @param \Magento\Catalog\Model\Product\LinkTypeProvider $linkTypeProvider
     * @param \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkFactory
     * @param \Magento\Catalog\Api\Data\ProductLinkExtensionFactory $productLinkExtensionFactory
     * @param \Magento\Catalog\Model\Product\Attribute\Backend\Media\EntryConverterPool $mediaGalleryEntryConverterPool
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $joinProcessor
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataService,
        \Magento\Catalog\Model\Product\Url $url,
        \Magento\Catalog\Model\Product\Link $productLink,
        \Magento\Catalog\Model\Product\Configuration\Item\OptionFactory $itemOptionFactory,
        \Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory $stockItemFactory,
        \Magento\Catalog\Model\Product\OptionFactory $catalogProductOptionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $catalogProductStatus,
        \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig,
        \Magento\Catalog\Model\Product\Type $catalogProductType,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Catalog\Helper\Product $catalogProduct,
        \Magento\Catalog\Model\ResourceModel\Product $resource,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $resourceCollection,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Indexer\IndexerRegistry $indexerRegistry,
        \Magento\Catalog\Model\Indexer\Product\Flat\Processor $productFlatIndexerProcessor,
        \Magento\Catalog\Model\Indexer\Product\Price\Processor $productPriceIndexerProcessor,
        \Magento\Catalog\Model\Indexer\Product\Eav\Processor $productEavIndexerProcessor,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Model\Product\Image\CacheFactory $imageCacheFactory,
        \Magento\Catalog\Model\ProductLink\CollectionProvider $entityCollectionProvider,
        \Magento\Catalog\Model\Product\LinkTypeProvider $linkTypeProvider,
        \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Catalog\Api\Data\ProductLinkExtensionFactory $productLinkExtensionFactory,
        \Magento\Catalog\Model\Product\Attribute\Backend\Media\EntryConverterPool $mediaGalleryEntryConverterPool,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $joinProcessor,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->resourceConnection = $resourceConnection;
        $this->logger = $context->getLogger();
        $this->registry = $registry;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $storeManager,
            $metadataService,
            $url,
            $productLink,
            $itemOptionFactory,
            $stockItemFactory,
            $catalogProductOptionFactory,
            $catalogProductVisibility,
            $catalogProductStatus,
            $catalogProductMediaConfig,
            $catalogProductType,
            $moduleManager,
            $catalogProduct,
            $resource,
            $resourceCollection,
            $collectionFactory,
            $filesystem,
            $indexerRegistry,
            $productFlatIndexerProcessor,
            $productPriceIndexerProcessor,
            $productEavIndexerProcessor,
            $categoryRepository,
            $imageCacheFactory,
            $entityCollectionProvider,
            $linkTypeProvider,
            $productLinkFactory,
            $productLinkExtensionFactory,
            $mediaGalleryEntryConverterPool,
            $dataObjectHelper,
            $joinProcessor
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        $productNamePriority = $this->scopeConfig->getValue('icecat_root/icecat/name_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($productNamePriority == 'Db') {
            return parent::getName();
        }

        try {
            self::$_product_source = '';
            $connection = $this->resourceConnection->getConnection('core_read');
            $entity_id = $this->getId();

            $manufacturerId = $this->getData($this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            $mpn = $this->getData($this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            $ean_code = $this->getData($this->scopeConfig->getValue('icecat_root/icecat/ean_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));

            $attributeInfo = $this->objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection::class)
                ->setCodeFilter($this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
                ->setEntityTypeFilter($this->getResource()->getTypeId())
                ->getFirstItem();
            switch ($attributeInfo->getData('backend_type')) {
                case 'int':
                    $attribute = $attributeInfo->setEntity($this->getResource());
                    $manufacturer = $attribute->getSource()->getOptionText($manufacturerId);
                    break;
                default:
                    $manufacturer = $manufacturerId;
                    break;
            }
            if(!empty($entity_id)){
                $tableName = $this->resourceConnection->getTableName('iceshop_icecatlive_products_titles');
                $selectCondition = $connection->select()
                    ->from(array('connector' => $tableName), new \Zend_Db_Expr('connector.prod_title'))
                    ->where('connector.prod_id = ? ', $entity_id);
                $icecatName = $connection->fetchOne($selectCondition);
            }
        } catch (\Exception $e) {
            $this->logger->debug('Icecat getName error' . $e);
        }
        $product_name = !empty($icecatName) ? $icecatName : parent::getName();

        return $product_name;
    }


    public function getImage()
    {
        if (!parent::getImage() || parent::getImage() == 'no_selection') {
            return "true";
        } else {
            return parent::getImage();
        }
    }

    public function getShortDescription()
    {

        if (!isset(self::$_product_source)) {
            $this->checkIcecatProdDescription();
        }

        $source = self::$_product_source;

        if ('Icecat' == $this->scopeConfig->getValue('icecat_root/icecat/descript_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) and $source != 'DB') {
            return true;
        } else {
            return parent::getShortDescription();
        }
    }

    public function getDescription()
    {

        if (!isset(self::$_product_source)) {
            $this->checkIcecatProdDescription();
        }

        $source = self::$_product_source;


        if ('Icecat' == $this->scopeConfig->getValue('icecat_root/icecat/descript_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) and  $source != 'DB') {
            return true;
        } else {
            return parent::getDescription();
        }
    }

    public function checkIcecatProdDescription($productId = '', $attributeName = '')
    {

        $iceImport = $this->objectManager->create(\ICEShop\Icecatlive\Model\Import::class);

        if (empty($productId)) {
            $productId = $this->registry->registry('current_product')->getId();
        }

        $_product = $this->load($productId);

        $mpn = $_product->getData($this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $ean_code = $_product->getData($this->scopeConfig->getValue('icecat_root/icecat/ean_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $manufacturerId = $_product->getData($this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $attributeInfo = $this->objectManager->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection::class)
            ->setCodeFilter($this->scopeConfig->getValue('icecat_root/icecat/manufacturer', \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
            ->setEntityTypeFilter($this->getResource()->getTypeId())
            ->getFirstItem();

        if ($attributeInfo->getData('backend_type') == 'int' ||
            ($attributeInfo->getData('frontend_input') == 'select' && $attributeInfo->getData('backend_type') == 'static')
        ) {
            $attribute = $attributeInfo->setEntity($_product->getResource());
            $manufacturer = $attribute->getSource()->getOptionText($manufacturerId);
        } else {
            $manufacturer = $manufacturerId;
        }
        $locale = $this->scopeConfig->getValue('icecat_root/icecat/language', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($locale == '0') {
            $systemLocale = explode("_", $this->objectManager->get('\Magento\Framework\Locale\Resolver')->getLocale());
            $locale = $systemLocale[0];
        }
        $userLogin = $this->scopeConfig->getValue('icecat_root/icecat/login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $userPass = $this->scopeConfig->getValue('icecat_root/icecat/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $entityId = $_product->getEntityId();

        $descr[1] = false;
        $descr[0] = $iceImport->getProductDescription($mpn, $manufacturer, $locale, $userLogin, $userPass, $entityId, $ean_code);

        if (!empty($iceImport->simpleDoc)) {
            $productTag = $iceImport->simpleDoc->Product;
            $descr[1] = (string)$productTag->ProductDescription['ShortDesc'];
        } else if (!empty($descr[0]) && $attributeName == 'short_description') {
            $descr[1] = $iceImport->getShortProductDescription();
        }
        if ($descr[0] == false and $descr[1] == false) {
            self::$_product_source = 'DB';
        } else if ($attributeName == 'short_description') {
            self::$_product_source = '';
            return $descr[1];
        } else if ($attributeName == 'name') {
            self::$_product_source = '';
            return $iceImport->getProductName();
        } else {
            self::$_product_source = '';
        }
    }

    const ENTITY = 'catalog_product';
    const CACHE_TAG = 'catalog_product';
    protected $_cacheTag = 'catalog_product';
    protected $_eventPrefix = 'catalog_product';
    protected $_eventObject = 'product';
    protected $_canAffectOptions = false;

    /**
     * Product type instance
     *
     * @var \Magento\Catalog\Model\Product\Type\AbstractType
     */
    protected $_typeInstance = null;

    /**
     * Product type instance as singleton
     */
    protected $_typeInstanceSingleton = null;

    /**
     * Product link instance
     *
     * @var \Magento\Catalog\Model\Product\Link
     */
    protected $_linkInstance;

    /**
     * Product object customization (not stored in DB)
     *
     * @var array
     */
    protected $_customOptions = [];

    /**
     * Product Url Instance
     *
     * @var \Magento\Catalog\Model\Product\Url
     */
    protected $_urlModel = null;
    protected static $_url;
    protected static $_urlRewrite;
    protected $_errors = [];
    protected $_optionInstance;
    protected $_options = [];

    /**
     * Product reserved attribute codes
     */
    protected $_reservedAttributes;

    /**
     * Flag for available duplicate function
     *
     * @var boolean
     */
    protected $_isDuplicable = true;

    /**
     * Source of product data
     *
     * @var string
     */
    public static $_product_source = '';

}