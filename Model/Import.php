<?php

namespace ICEShop\Icecatlive\Model;

use Magento\Framework\Config\ConfigOptionsListConstants;

/**
 * Class Import
 * @package ICEShop\Icecatlive\Model
 */
class Import extends \Magento\Framework\Model\AbstractModel
{

    public $entityId;
    private $productDescriptionList = [];
    private $productDescription;
    private $fullProductDescription;
    private $lowPicUrl;
    private $highPicUrl;
    private $errorMessage;
    private $productName;
    private $relatedProducts = [];
    private $thumb;
    private $errorSystemMessage;
    public $_cacheKey = 'iceshop_icecatlive_';
    public $_connectorCacheDir = '/iceshop/icecatlive/cache/';
    public $simpleDoc;

    private $_warrantyInfo = '';
    private $_shortSummaryDesc = '';
    private $_longSummaryDesc = '';

    private $_manualPdfUrl = '';
    private $_pdfUrl = '';
    private $_multimedia = '';

    public $productId = '';
    public $EAN = '';
    public $vendor = '';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $catalogProductMediaConfig;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    public $objectManager;

    /**
     * @var string
     */
    public $tablePrefix;

    public $product_entity_type_id;

    public $attribute_set_id;

    public $gallery_id;

    public $base_id;

    public $small_id;

    public $thumb_id;

    public $_request;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Model\Product $catalogProductFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {

        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $deployConfig = $this->objectManager->get('Magento\Framework\App\DeploymentConfig');
        $this->tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);
        $this->logger = $context->getLogger();
        $this->scopeConfig = $scopeConfig;
        $this->catalogProductMediaConfig = $catalogProductMediaConfig;
        $this->resourceConnection = $resourceConnection;
        $this->catalogProductFactory = $catalogProductFactory;
        $this->_request = $this->objectManager->get('\Magento\Framework\App\Request\Http');

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );

        $db = $this->resourceConnection->getConnection('core_write');

        $attr_query = $db->query("SELECT `entity_type_id` FROM `" . $this->tablePrefix . "eav_entity_type` WHERE
                                entity_type_code = 'catalog_product';");
        $this->product_entity_type_id = $attr_query->fetch()['entity_type_id'];

        $attr_query = $db->query("SELECT `entity_type_id` FROM `" . $this->tablePrefix . "eav_entity_type` WHERE
                                entity_type_code = 'catalog_product';");
        $this->attribute_set_id = $attr_query->fetch()['entity_type_id'];

        $attr_query = $db->query("SELECT `attribute_id` FROM `" . $this->tablePrefix . "eav_attribute` WHERE
                               `attribute_code` = 'media_gallery' AND entity_type_id = $this->product_entity_type_id;");
        $this->gallery_id = $attr_query->fetch()['attribute_id'];

        $attr_query = $db->query("SELECT `attribute_id` FROM `" . $this->tablePrefix . "eav_attribute` WHERE 
                                `attribute_code` = 'image' AND entity_type_id = $this->product_entity_type_id;");
        $this->base_id = $attr_query->fetch()['attribute_id'];

        $attr_query = $db->query("SELECT `attribute_id` FROM `" . $this->tablePrefix . "eav_attribute` WHERE
                               `attribute_code` = 'small_image' AND entity_type_id = $this->product_entity_type_id;");
        $this->small_id = $attr_query->fetch()['attribute_id'];

        $attr_query = $db->query("SELECT `attribute_id` FROM `" . $this->tablePrefix . "eav_attribute` WHERE
                               `attribute_code` = 'thumbnail' AND entity_type_id = $this->product_entity_type_id;");
        $this->thumb_id = $attr_query->fetch()['attribute_id'];
    }

    /**
     * Perform Curl request with corresponding param check and error processing
     * @param int $productId
     * @param string $vendorName
     * @param string $locale
     * @param string $userName
     * @param string $userPass
     */

    public function getProductDescription($productId, $vendorName, $locale, $userName, $userPass, $entityId, $ean_code)
    {
        $current_page = $this->_request->getFullActionName();

        if ($current_page == 'catalog_product_view') {
            $this->entityId = $entityId;
            if (null === $this->simpleDoc) {
                try {
                    $cacheDataXml = $this->_getXmlIcecatLiveCache($this->entityId, $locale);

                    if (!empty($cacheDataXml)) {

                        $resultString = $cacheDataXml;

                        if (!$this->parseXml($resultString)) {
                            return false;
                        }
                        if ($this->checkIcecatResponse($this->simpleDoc->Product['ErrorMessage'])) {
                            return false;
                        }
                    }

                    $this->loadProductDescriptionList();
                    $this->loadOtherProductParams($productId);
                    \Magento\Framework\Profiler::start('Iceshop FILE RELATED');
                    $this->loadRelatedProducts();
                    \Magento\Framework\Profiler::stop('Iceshop FILE RELATED');
                } catch (\Exception $e) {
                    $this->logger->debug("connector issue: {$e->getMessage()}");
                }
            }
            return true;
        }
    }

    public function _getXmlIcecatLiveCache($entity_id, $locale){
        $current_prodCacheXml =  BP.DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . $this->_connectorCacheDir . 'iceshop_icecatlive_' . $entity_id . '_' . $locale;
        if (file_exists($current_prodCacheXml)){
            $current_prodCache = file_get_contents($current_prodCacheXml);
            return $current_prodCache;
        } else {
            return false;
        }
    }
    public function _getIceCatData($userName, $userPass, $dataUrl, $productAttributes)
    {
        try {
            $webClient = new \Zend_Http_Client();
            $webClient->setUri($dataUrl);
            $webClient->setMethod(\Zend_Http_Client::GET);
            $webClient->setHeaders('Content-Type: text/xml; charset=UTF-8');
            $webClient->setParameterGet($productAttributes);
            $webClient->setAuth($userName, $userPass, \Zend_Http_CLient::AUTH_BASIC);
            $response = $webClient->request();
            if ($response->isError()) {
                $this->errorMessage = 'Response Status: ' . $response->getStatus() . " Response Message: " . $response->getMessage();
                return false;
            }
        } catch (\Exception $e) {
            $this->errorMessage = "Warning: cannot connect to ICEcat. {$e->getMessage()}";
            return false;
        }
        return $response->getBody();
    }

    public function getSystemError()
    {
        return $this->errorSystemMessage;
    }

    public function getProductName()
    {
        return $this->productName;
    }
    public function getThumbPicture()
    {
        return $this->thumb;
    }
    /**
     * load Gallery array from XML
     */
    public function loadGalleryPhotos($productId, $galleryPhotos)
    {
        $imagepriority = $this->scopeConfig->getValue('icecat_root/icecat/image_priority', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if($imagepriority == 'Db') {
            return true;
        }

        if (!count($galleryPhotos)) {
            return true;
        }

        foreach ($galleryPhotos as $photo) {

            $isMain = (isset($photo["IsMain"]) && $photo["IsMain"] == 'Y') ? true : false;
            if($isMain) {

                $iceCatImagesAttributes = [
                    'HighPic',
                    'Pic',
                    'LowPic',
                    'ThumbPic',
                ];

                $imageUrl = '';
                foreach($iceCatImagesAttributes as $imageAttribute) {
                    $xmlImageAttribute = isset($photo[$imageAttribute]) ? (string)$photo[$imageAttribute] : '';

                    if(!strlen($imageUrl) && strlen($xmlImageAttribute)) {
                        $imageUrl = $xmlImageAttribute;
                    }
                }

                if(!strlen($imageUrl)) {
                    continue;
                }

                $this->addImage($productId, $this->loadImage($imageUrl), true);

                continue;
            } else {

                if ($photo["Size"] > 0) {
                    $imgUrl = (string)$photo["Pic"];

                    if (!empty($imgUrl) && (strpos($imgUrl, 'feature_logo') + 1) === 1) {
                        $this->addImage($productId, $this->loadImage($imgUrl));
                    }
                }

            }

        }

        return true;
    }

    /**
     * Change request host for image parse https to http
     * @param string $productTag
     * @return string
     */
    public function changeHostsImages($productTag){
        $image_path = str_replace("http:", "https:", $productTag);
        return $image_path;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Checks response XML for error messages
     */
    public function checkIcecatResponse($errorMessage)
    {
        if ($errorMessage != '') {
            if (preg_match('/^No xml data/', $errorMessage)) {
                $this->errorSystemMessage = (string)$errorMessage;
                return true;
            }
            if (preg_match('/^The specified vendor does not exist$/', $errorMessage)) {
                $this->errorSystemMessage = (string)$errorMessage;
                return true;
            }
            if (preg_match('/^You are not allowed to have Full ICEcat access$/', $errorMessage)) {
                $this->errorMessage = "Warning: " . $errorMessage;
                return true;
            }
            $this->errorMessage = "Ice Cat Error: " . $errorMessage;
            return true;
        }
        return false;
    }

    public function getProductDescriptionList()
    {
        return $this->productDescriptionList;
    }

    public function getShortProductDescription()
    {
        return $this->productDescription;
    }

    public function getFullProductDescription()
    {
        return $this->fullProductDescription;
    }

    public function getLowPicUrl()
    {
        return $this->highPicUrl;
    }

    public function getRelatedProducts()
    {
        return $this->relatedProducts;
    }

    public function getVendor()
    {
        return $this->vendor;
    }

    public function getMPN()
    {
        return $this->productId;
    }

    public function getEAN()
    {
        return $this->EAN;
    }

    public function getWarrantyInfo()
    {
        return $this->_warrantyInfo;
    }

    public function getShortSummaryDescription()
    {
        return $this->_shortSummaryDesc;
    }

    public function getLongSummaryDescription()
    {
        return $this->_longSummaryDesc;
    }

    public function getManualPDF()
    {
        return $this->_manualPdfUrl;
    }

    public function getPDF()
    {
        return $this->_pdfUrl;
    }

    public function getIceCatMedia()
    {
        return $this->_multimedia;
    }

    /**
     * Form related products Array
     */
    private function loadRelatedProducts()
    {
        if(!empty($this->simpleDoc) && is_object($this->simpleDoc)){
            if(!empty($this->simpleDoc->Product) && is_object($this->simpleDoc->Product)){
                $relatedProductsArray = $this->simpleDoc->Product->ProductRelated;
                if (count($relatedProductsArray)) {
                    foreach ($relatedProductsArray as $product) {
                        $productArray = [];
                        $productNS = $product->Product;
                        $productArray['name'] = (string)$productNS['Name'];
                        $productArray['thumb'] = (string)$productNS['ThumbPic'];
                        $mpn = (string)$productNS['Prod_id'];
                        $productSupplier = $productNS->Supplier;
                        $productSupplierId = (int)$productSupplier['ID'];
                        $productArray['supplier_thumb'] = 'http://images2.icecat.biz/thumbs/SUP' . $productSupplierId . '.jpg';
                        $productArray['supplier_name'] = (string)$productSupplier['Name'];
                        $this->relatedProducts[$mpn] = $productArray;
                    }
                }
            }
        }
    }

    /**
     * Form product feature Array
     */
    public function loadProductDescriptionList()
    {

        try{
            $descriptionArray = [];
            if(is_object($this->simpleDoc) && !empty($this->simpleDoc)){
                if(is_object($this->simpleDoc->Product) && !empty($this->simpleDoc->Product)){
                    $specFeatures = $this->simpleDoc->Product->ProductFeature;
                    $specGroups = $this->simpleDoc->Product->CategoryFeatureGroup;
                    foreach ($specFeatures as $feature) {
                        $id = (int)$feature['CategoryFeatureGroup_ID'];
                        $featureText = (string)$feature["Presentation_Value"];
                        $featureValue = (string)$feature["Value"];
                        $featureName = (string)$feature->Feature->Name["Value"];
                        if ($featureValue == 'Y' || $featureValue == 'N') {
                            $featureText = $featureValue;
                        }
                        foreach ($specGroups as $group) {
                            $groupId = (int)$group["ID"];
                            if ($groupId == $id) {
                                $groupName = (string)$group->FeatureGroup->Name["Value"];
                                $rating = (int)$group['No'];
                                $descriptionArray[$rating][$groupName][$featureName] = $featureText;
                                break;
                            }
                        }
                    }
                }
            }
            krsort($descriptionArray);
            $this->productDescriptionList = $descriptionArray;
        } catch (\Exception $e){
            $this->logger->debug("connector issue: {$e->getMessage()}");
            $this->productDescriptionList = [];
        }
    }

    /**
     * Form Array of non feature-value product params
     */
    private function loadOtherProductParams($productId)
    {
        if(!empty($this->simpleDoc) && is_object($this->simpleDoc)) {
            $productTag = $this->simpleDoc->Product;
        }

        if(!empty($productTag) && is_object($productTag)) {
            $this->productDescription = (string)$productTag->ProductDescription['ShortDesc'];
            $this->fullProductDescription = (string)$productTag->ProductDescription['LongDesc'];
            $this->_warrantyInfo = (string)$productTag->ProductDescription['WarrantyInfo'];
            $this->_shortSummaryDesc = (string)$productTag->SummaryDescription->ShortSummaryDescription;
            $this->_longSummaryDesc = (string)$productTag->SummaryDescription->LongSummaryDescription;
            $this->_manualPdfUrl = (string)$productTag->ProductDescription['ManualPDFURL'];
            $this->_pdfUrl = (string)$productTag->ProductDescription['PDFURL'];
            $this->_multimedia = $productTag->ProductMultimediaObject->MultimediaObject;
            $this->productName = (string)$productTag["Title"];
            $this->productId = (string)$productTag['Prod_id'];
            $this->vendor = (string)$productTag->Supplier['Name'];
            $prodEAN = $productTag->EANCode;
            $EANstr = '';
            $EANarr = null;
            $j = 0; //counter
            foreach ($prodEAN as $ellEAN) {
                $EANarr[] = $ellEAN['EAN'];
                $j++;
            }
            $i = 0;
            $str = '';
            for ($i = 0; $i < $j; $i++) {
                $g = $i % 2;
                if ($g == '0') {
                    if ($j == 1) {
                        $str .= $EANarr[$i] . '<br>';
                    } else {
                        $str .= $EANarr[$i] . ', ';
                    }
                } else {
                    if ($i != $j - 1) {
                        $str .= $EANarr[$i] . ', <br>';
                    } else {
                        $str .= $EANarr[$i] . ' <br>';
                    }
                }
            }
            $this->EAN = $str;
        }
    }

    /**
     * parse response XML: to SimpleXml
     * @param string $stringXml
     */
    public function parseXml($stringXml)
    {
        $current_page = $this->_request->getFullActionName();

        if ($current_page == 'catalog_product_view') {
            libxml_use_internal_errors(true);
            $this->simpleDoc = simplexml_load_string($stringXml);
            if ($this->simpleDoc) {
                return true;
            }
            $this->simpleDoc = simplexml_load_string(utf8_encode($stringXml));
            if ($this->simpleDoc) {
                return true;
            }
        }

        return false;
    }

    public function loadImage($imgUrl)
    {
        $picUrl = $this->changeHostsImages($imgUrl);

        $pathinfo = pathinfo($imgUrl);
        if(!empty($pathinfo["extension"])){
            $imgType = $pathinfo["extension"];
        }

        if (strpos($imgUrl, 'high') === true) {
            $imgName = str_replace("http://images.icecat.biz/img/norm/high/", "", $imgUrl);
            $imgName = hash("md5",$imgName);
        } else if (strpos($imgUrl, 'low') === true) {
            $imgName = str_replace("http://images.icecat.biz/img/norm/low/", "", $imgUrl);
            $imgName = hash("md5",$imgName);
        } else {
            $imgName = hash("md5",$imgUrl);
        }

        $imgFullName = $imgName . "." . $imgType;

        $baseDir = BP.DIRECTORY_SEPARATOR .'pub'.DIRECTORY_SEPARATOR.'media'.DIRECTORY_SEPARATOR.$this->catalogProductMediaConfig->getBaseMediaPath() . '/';
        $localImg = strstr($imgUrl, $this->scopeConfig->getValue('web/unsecure/base_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));

        if (!file_exists($baseDir . $imgFullName) && !$localImg) {
            $content = (new \Zend_Http_Client($imgUrl))->request();
            if ($content->isError()) {
                return false;
            }

            $file = file_put_contents($baseDir . $imgFullName, $content->getBody());

            if (!$file) {
                return false;
            }
        }

        return $imgFullName;
    }

    public function addImage($productId, $imageName, $base = false) {
        if(!$imageName) {
            return false;
        }

        $dir = $this->objectManager->get('Magento\Framework\Filesystem');
        $directory_list = $this->objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
        $mediaDir = $directory_list->getPath('media');
        $mediaPath = $this->catalogProductMediaConfig->getBaseMediaPath();

        $product = $this->objectManager->create('\Magento\Catalog\Model\Product')->load(
            $productId
        );

        $reAdd = $this->scopeConfig->getValue('icecat_root/icecat/product_redownloadimages', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $imageProcessor = $this->objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');

        // Check image allready exists
        $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
        if(is_array($existingMediaGalleryEntries)) {
            foreach ($existingMediaGalleryEntries as $key => $entry) {
                $oldImage = $entry->getFile();
                $oldImage = explode( "/", $oldImage );
                $oldImage = end($oldImage);

                if($oldImage == $imageName) {
                    if ($reAdd) {
                        $image = $entry->getFile();
                        $imageProcessor->removeImage($product, $image);
                        $product->save();

                        break;

                    } else {
                        return false;
                    }
                }
            }
        }

        $types = ['images'];
        if($base) {
            $types = ['small_image', 'thumbnail', 'image'];
        }

        $product = $this->objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
        $product->addImageToMediaGallery($mediaDir . DIRECTORY_SEPARATOR . $mediaPath . DIRECTORY_SEPARATOR. $imageName, $types, false, false, true);
        $product->save();
        unset($product);

    }
}