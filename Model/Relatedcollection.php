<?php

namespace ICEShop\Icecatlive\Model;

/**
 * Class Relatedcollection
 * @package ICEShop\Icecatlive\Model
 */
class Relatedcollection extends \Magento\Framework\Data\Collection
{

    protected $_data = [];
    protected $_collection;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * Relatedcollection constructor.
     */
    public function __construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $entityFactory = $objectManager->get('\Magento\Framework\Data\Collection\EntityFactoryInterface');
        $this->catalogProductFactory = $objectManager->get('\Magento\Catalog\Model\Product');
        parent::__construct(
            $entityFactory
        );

        $args = func_get_args();

        if (empty($args[0])) {
            $args[0] = [];
        }
        $this->_data = $args[0];
    }

    public function getCollection()
    {
        $sku = $this->scopeConfig->getValue('icecat_root/icecat/sku_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $model = $this->catalogProductFactory;
        $collection = $model->getCollection();

        $filterArray = [];
        $rel = [];
        foreach ($this->_data as $res) {
            foreach ($res as $r) {
                $rel[] = $r;
            }
        }

        foreach ($rel as $item) {
            array_push($filterArray, array('attribute' => $sku, 'eq' => $item['mpn']));
        }
        $collection->addFieldToFilter($filterArray);

        $collection->joinField(
            'is_in_stock',
            'cataloginventory_stock_item',
            'is_in_stock',
            'product_id=entity_id',
            '{{table}}.stock_id=1',
            'left'
        );

        $myCollection = clone $collection;
        $relCnt = count($rel);
        foreach ($myCollection as &$col) {
            $model->load($col->getId());
            $price = $model->getPrice();
            $mpn = $col->getData($sku);
            $specialPrice = $model->getSpecialPrice();

            for ($i = 0; $i < $relCnt; $i++) {
                if ($rel[$i]['mpn'] == $mpn) {
                    $col->setData('name', $rel[$i]['name']);
                    $col->setData('thumbnail', $rel[$i]['thumb']);
                    $col->setData('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                    $col->setData('price', $price);
                    $col->setData('special_price', $specialPrice);
                }
            }
        }
        return $collection;
    }
}
