<?php

namespace ICEShop\Icecatlive\Model\System\Config;

use Magento\Framework\App\ObjectManager;

/**
 * Class Attributes
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Attributes implements \Magento\Framework\Option\ArrayInterface
{

    public $urlBuilder;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $return = [
            '' => "--- " . __('Choose attribute') . " ---"
        ];

        $coll = ObjectManager::getInstance()
            ->create(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection::class);

        $coll->addFieldToFilter(\Magento\Eav\Model\Entity\Attribute\Set::KEY_ENTITY_TYPE_ID, 4);
        $attrAll = $coll->load()->getItems();

        if (!empty($attrAll)) {
            foreach ($attrAll as $key => $value) {
                $return[$value->getData('attribute_code')] = $value->getData('attribute_code');
            }
        }

        asort($return);

        return $return;
    }
}