<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Descriptionpriority
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Descriptionpriority
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            'Icecat' => 'From Icecat',
            'Db' => 'From Database'
        );

    }
}
