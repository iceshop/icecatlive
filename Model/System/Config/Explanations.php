<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Explanations
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Explanations
{

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $backendUrlInterface;

    /**
     * Explanations constructor.
     * @param \Magento\Backend\Model\UrlInterface $backendUrlInterface
     */
    public function __construct(
        \Magento\Backend\Model\UrlInterface $backendUrlInterface
    ) {
        $this->backendUrlInterface = $backendUrlInterface;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            0 => base64_encode($this->backendUrlInterface->getUrl("adminhtml/icecatlive/explanations/"))
        );
    }
}