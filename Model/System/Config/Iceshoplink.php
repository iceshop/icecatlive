<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Iceshoplink
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Iceshoplink
{

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            0 => base64_encode('http://www.iceshop.biz/contact/')
        );
    }
}