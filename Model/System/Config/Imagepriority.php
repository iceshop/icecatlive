<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Imagepriority
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Imagepriority
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            'Icecat' => 'From Icecat',
            'Db' => 'From Database'
        );

    }
}
