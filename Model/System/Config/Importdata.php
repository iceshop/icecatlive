<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Importdata
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Importdata
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            'label' => 'Import'
        );

    }
}