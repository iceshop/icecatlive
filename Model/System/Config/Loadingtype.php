<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Loadingtype
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Loadingtype
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            '0' => 'From cache',
            '1' => "Realtime (If cache doesn't exist)"
        );

    }
}
