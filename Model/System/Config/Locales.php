<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Locales
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Locales
{

    private $domDoc;

    /**
     * @return array|bool
     */
    public function toOptionArray()
    {
        $pathToFile = dirname(__FILE__).'/';
        $fileContent = file_get_contents($pathToFile . 'LanguageList.xml');
        if (!$this->parseXml(utf8_encode($fileContent))) {
            return false;
        }

        $values = $this->parseLocaleValues();
        return $values;
    }

    /**
     * @return array
     */
    private function parseLocaleValues()
    {
        $languageArray = $this->domDoc->getElementsByTagName('Language');
        $resultArray = [];
        foreach ($languageArray as $language) {
            $languageShortCode = strtolower($language->getAttribute('ShortCode'));
            $languageCode = ucfirst($language->getAttribute('Code'));
            $resultArray[$languageShortCode] = $languageCode;
        }
        ksort($resultArray);
        array_unshift($resultArray, 'Use Store Locale');
        return $resultArray;
    }

    private function parseXml($stringXml)
    {
        $this->domDoc = new \DOMDocument();
        $result = $this->domDoc->loadXML($stringXml);
        return true;
    }
}
