<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Namepriority
 * @package ICEShop\Icecatlive\Model\System\Config
 */
class Namepriority
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            'Icecat' => 'From Icecat',
            'Db' => 'From Database'
        );

    }
}
