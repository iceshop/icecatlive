<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Productpriority
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Productpriority
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            'Show' => 'Show all products',
            'Hide' => 'Hide products not from Icecat'
        );

    }
}
