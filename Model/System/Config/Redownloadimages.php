<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Onlynewproducts
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Redownloadimages
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            '0' => "No",
            '1' => 'Yes'
        );

    }
}