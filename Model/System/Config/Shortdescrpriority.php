<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Shortdescrpriority
 * @package ICEShop\Icecatlive\Model\System\Config
 */

class Shortdescrpriority
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            'Icecat' => 'From Icecat',
            'Db' => 'From Database'
        );

    }
}
