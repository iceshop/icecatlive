<?php

namespace ICEShop\Icecatlive\Model\System\Config;

/**
 * Class Subscription
 * @package ICEShop\Icecatlive\Model\System\Config
 */
class Subscription
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $paramsArray = array(
            'free' => 'OpenIcecat XML',
            'full' => 'FullIcecat XML'
        );
        return $paramsArray;
    }
}
