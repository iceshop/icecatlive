<?php

namespace ICEShop\Icecatlive\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Config\ConfigOptionsListConstants;

/**
 * Class UpgradeSchema
 * @package ICEShop\Icecatlive\Setup
 */

class UpgradeSchema implements UpgradeSchemaInterface
{
    /*
     * Connection variable
     */
    private $connection = null;

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $deployConfig = $objectManager->get('Magento\Framework\App\DeploymentConfig');
        $tablePrefix = (string)$deployConfig->get(ConfigOptionsListConstants::CONFIG_PATH_DB_PREFIX);

        $setup->startSetup();

        $this->_getConnection();

        $sql = "DROP TABLE IF EXISTS `bintime_connector_data`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `bintime_connector_data_old`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `bintime_connector_data_products`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `bintime_supplier_mapping`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `bintime_supplier_mapping_old`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `iceshop_icecatlive_connector_data`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `iceshop_icecatlive_supplier_mapping`;";
        $this->connection->query($sql);

        $sql = "DROP TABLE IF EXISTS `iceshop_icecatlive_connector_data_products`;";
        $this->connection->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `iceshop_icecatlive_noimport_products_id` (
	`prod_id` INT(255) UNSIGNED NOT NULL,
   UNIQUE KEY (`prod_id`)
	) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Iceshop Connector not import products from icecat';";
        $this->connection->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `iceshop_icecatlive_products_titles` (
		`prod_id` INT(255) UNSIGNED NOT NULL,
        `prod_title` VARCHAR(255) NULL DEFAULT NULL,
        UNIQUE KEY (`prod_id`),
        FOREIGN KEY (prod_id)
            REFERENCES {$tablePrefix}catalog_product_entity(entity_id)
            ON DELETE CASCADE
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Iceshop Connector product titles';";
        $this->connection->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `iceshop_icecatlive_extensions_logs`(
	`log_key` VARCHAR(255) NOT NULL,
	`log_value` TEXT DEFAULT NULL,
	UNIQUE KEY (`log_key`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Iceshop Connector logs';";
        $this->connection->query($sql);

        $setup->endSetup();
    }

    private function _getConnection()
    {
        if (!$this->connection) {
            $resource = ObjectManager::getInstance()->create('\Magento\Framework\App\ResourceConnection');
            $this->connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->connection;
    }
}
